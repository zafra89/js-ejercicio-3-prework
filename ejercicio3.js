function comprobarDNI() {
  var letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 
							'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];

  var numeroDNI = prompt("Indique su número de DNI: ");
  var letraDNI = prompt("Ahora indique su letra de DNI: ");
  var index = numeroDNI % 23;

  if (letraDNI === letras[index]) {
    console.log("DNI Válido")
  } else {
    console.log("DNI Incorrecto")
  }
}

comprobarDNI();